<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () {
	CRUD::resource('pages', 'PagesCrudController');
	CRUD::resource('brands', 'BrandsCrudController');
	CRUD::resource('certificates', 'CertificatesCrudController');
	CRUD::resource('slides', 'SlidesCrudController');
	CRUD::resource('settings', 'SettingsCrudController');
});
