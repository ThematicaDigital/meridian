<?php
Breadcrumbs::register('home', function ($breadcrumbs) {
	$breadcrumbs->push('Главная', action('HomeController@index'));
});

Breadcrumbs::register('page', function ($breadcrumbs, $page) {
	$breadcrumbs->parent('home');
	$breadcrumbs->push($page->title, action('PagesController@view', $page->slug));
});

Breadcrumbs::register('contacts', function ($breadcrumbs) {
	$breadcrumbs->parent('home');
	$breadcrumbs->push('Контакты', action('HomeController@contacts'));
});