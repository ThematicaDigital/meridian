<?php

use Illuminate\Database\Seeder;
use App\Models\Slides;

class SlidesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Slides::truncate();
        factory(Slides::class, 5)->create();
    }
}
