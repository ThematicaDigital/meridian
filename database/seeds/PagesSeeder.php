<?php

use Illuminate\Database\Seeder;
use App\Models\Pages;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pages::truncate();
        factory(Pages::class)->create(['title' => 'О компании', 'slug' => 'about']);
        factory(Pages::class)->create(['title' => 'Продукция', 'slug' => 'products']);
        factory(Pages::class)->create(['title' => 'Поставщикам', 'slug' => 'for-suppliers']);
        factory(Pages::class)->create(['title' => 'Покупателям', 'slug' => 'for-buyers']);
        factory(Pages::class)->create(['title' => 'Оплата', 'slug' => 'payment']);
        factory(Pages::class)->create(['title' => 'Информация', 'slug' => 'info']);
        factory(Pages::class)->create(['title' => 'Текст на главной', 'slug' => 'main', 'text' => '
            <div class="grid-container margin-vertical-3">
                <div class="grid-x grid-padding-x">
                    <div class="cell small-12 medium-7 margin-bottom-1">
                        <article>
                            <h1>ООО &laquo;Меридиан&raquo;</h1>
                            <p>Наш сайт является онлайн-площадкой компании ООО «Меридиан», и предназначен для ведения совместного бизнеса в сфере снабжения продуктами питания предприятий общепита (ресторанов, баров, кафе, столовых и т.д), а также для более удобного общения с нашими партнёрами и контрагентами.</p>
                            <p><strong>Наши преимущества:</strong></p>
                            <p>
                                <ul>
                                    <li>большой выбор продуктов питания для предприятий общественного питания;</li>
                                    <li>взаимовыгодная система скидок и спецпредложений для постоянных закупщиков;</li>
                                    <li>оперативная помощь в комплектации заявок на снабжение продуктами;</li>
                                    <li>оптовая и розничная продажа продуктов питания по всей Москве и области;</li>
                                    <li>доставка продуктов в рестораны, бары, кафе, столовые.</li>
                                </ul>
                            </p> 
                            <p>Наш сайт является онлайн-площадкой компании ООО «Меридиан», и предназначен для ведения совместного бизнеса в сфере снабжения продуктами питания предприятий общепита (ресторанов, баров, кафе, столовых и т.д), а также для более удобного общения с нашими партнёрами и контрагентами.</p>
                        </article>
                    </div>
                    <div class="cell small-12 medium-5 margin-bottom-1 text-right">
                        <img src="/images/discount.jpg" >
                    </div>
                </div>
            </div>
        ']);
        factory(Pages::class)->create(['title' => 'Контакты', 'slug' => 'contacts', 'text' => '
            <div class="margin-bottom-2">
                <p class="margin-bottom-0">Бесплатный звонок по России</p>
                <a href="tel:88001001321" class="green-text">8-800-100-1-321</a>
            </div>
            <div class="margin-bottom-2">
                <p class="margin-bottom-1"><strong>Филиал в Москве</strong></p>
                <p class="margin-bottom-0">125009, г. Москва, Вознесенский пер, 11/1</p>
                <p class="margin-bottom-0">Тел./факс: +7(495)988-30-61</p>
                <p class="margin-bottom-0">Лицензия ЦБ РФ 1810</p>
            </div>
            <div class="margin-bottom-2">
                <p class="margin-bottom-1"><strong>Головной офис</strong></p>
                <p class="margin-bottom-0">675000, Амурская область,</p>
                <p class="margin-bottom-0">г. Благовещенск, ул. Амурская, 225</p>
            </div>
        ']);
    }
}
