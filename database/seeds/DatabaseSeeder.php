<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(SlidesSeeder::class);
        $this->call(BrandsSeeder::class);
        $this->call(CertificatesSeeder::class);
        $this->call(PagesSeeder::class);
    }
}
