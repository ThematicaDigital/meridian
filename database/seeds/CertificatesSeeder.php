<?php

use Illuminate\Database\Seeder;
use App\Models\Certificates;

class CertificatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Certificates::truncate();
        factory(Certificates::class, 20)->create();
    }
}
