<?php

use Illuminate\Database\Seeder;
use App\Models\Settings;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Settings::truncate();
        Settings::create([
        	'title' => 'Адрес компании',
        	'slug' => 'address',
        	'value' => 'г. Благовещенск, ул. Ленина 123, офис 7'
        ]);
        Settings::create([
        	'title' => 'Телефон компании',
        	'slug' => 'phone',
        	'value' => '8-800-100-1-321'
        ]);
    }
}