<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Certificates::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(),
        'image' => '/images/uploads/' . $faker->image('public/images/uploads/', 180, 120, 'cats', false),
    ];
});
