<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Pages::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(),
        'text' => $faker->paragraph(),
    ];
});
