<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Slides::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(),
        'image' => '/images/uploads/' . $faker->image('public/images/uploads/', 1440, 700, 'cats', false),
    ];
});
