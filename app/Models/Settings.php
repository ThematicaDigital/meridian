<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class Settings extends Model
{
    use SoftDeletes;
    use CrudTrait;

    protected $table = 'settings';
    protected $fillable = ['title', 'slug', 'value'];
}