<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class Brands extends Model
{
    use SoftDeletes;
    use CrudTrait;

    protected $table = 'brands';
    protected $fillable = ['title', 'image'];
}
