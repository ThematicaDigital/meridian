<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class Slides extends Model
{
    use SoftDeletes;
    use CrudTrait;

    protected $table = 'slides';
    protected $fillable = ['title', 'image'];
}
