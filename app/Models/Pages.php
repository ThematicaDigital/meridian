<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class Pages extends Model
{
    use SoftDeletes;
    use Sluggable;
    use CrudTrait;

    protected $table = 'pages';
    protected $fillable = ['title', 'image', 'text'];

    /**
     * Sluggable options
     *
     * @return array
     */
    public function sluggable()
	{
		return ['slug' => ['source' => 'title']];
	}

    /**
     * Get parent page
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function page()
    {
        return $this->belongsTo(Pages::class);
    }

    /**
     * Get pages
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function pages()
    {
        return $this->hasMany(Pages::class);
    }
}