<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DesignController extends Controller
{
    public function index()
    {
    	return view('design/index');
    }

    public function contacts()
    {
    	return view('design/contacts');
    }

    public function about()
    {
    	return view('design/about');
    }

    public function suppliers()
    {
    	return view('design/suppliers');
    }

    public function products()
    {
    	return view('design/products');
    }
}
