<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Certificates;

class CertificatesController extends Controller
{
    public static function widgetMain()
    {
    	$certificates = Certificates::select(['title', 'image'])->get();
    	return view('certificates/widget-main', compact('certificates'));
    }
}
