<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Pages;

class PagesController extends Controller
{
    public function view($slug)
	{
		$page = Pages::where('slug', $slug)->first();
		return view('pages/view', compact('page'));
	}

    public static function preview($slug)
	{
		$page = Pages::where('slug', $slug)->first();
		return view('pages/preview', compact('page'));
	}
}
