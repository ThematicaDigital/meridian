<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the home page
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Show the contacts page
     *
     * @return \Illuminate\Http\Response
     */
    public function contacts()
    {
        return view('contacts');
    }
}
