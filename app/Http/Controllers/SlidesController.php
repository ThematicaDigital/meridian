<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Slides;

class SlidesController extends Controller
{
    public static function widgetMain()
    {
    	$slides = Slides::select(['image as background'])->get()->map(function($item, $key) {
			$item->background = "url('/{$item->background}') no-repeat 50% 50%";
			return $item;
		});
    	return view('slides/widget-main', compact('slides'));
    }
}