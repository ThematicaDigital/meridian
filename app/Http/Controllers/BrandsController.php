<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brands;

class BrandsController extends Controller
{
    public static function widgetMain()
    {
    	$brands = Brands::select(['title', 'image'])->get();
    	return view('brands/widget-main', compact('brands'));
    }
}
