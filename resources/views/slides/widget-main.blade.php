@if (!$slides->isEmpty())
	<div class="slider margin-0">
		<sliderblock slides="{{ $slides->toJson() }}"></sliderblock>
	</div>
@endif