<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>Адми-панель</span></a></li>
<li><a href="{{ backpack_url('pages') }}"><i class="fa fa-file"></i> <span>Статичные страницы</span></a></li>
<li><a href="{{ backpack_url('brands') }}"><i class="fa fa-list-ul"></i> <span>Бренды</span></a></li>
<li><a href="{{ backpack_url('certificates') }}"><i class="fa fa-certificate"></i> <span>Сертификаты</span></a></li>
<li><a href="{{ backpack_url('slides') }}"><i class="fa fa-image"></i> <span>Слайды</span></a></li>
<li><a href="{{ backpack_url('settings') }}"><i class="fa fa-cogs"></i> <span>Настройки</span></a></li>
<li><a href="{{  backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>Менеджер файлов</span></a></li>