<footer>
	<div class="yellow-container padding-vertical-2">
		<div class="grid-container">
			<div class="grid-x">
				<div class="cell small-6 medium-2 margin-bottom-2">
					<a href="{{ route('home') }}" title="Эко Мир">
	            		<img src="/images/logotype.svg" alt="Эко Мир" title="Эко Мир"/>
	            	</a>
				</div>
				<div class="cell small-6 medium-2 margin-bottom-2">
					<h6 class="margin-bottom-1">О нас</h6>
					<ul class="menu vertical">
						<li><a href="{{ route('page.view', ['about']) }}" title="О компании">О компании</a></li>
						<li><a href="{{ route('contacts') }}" title="Контакты">Контакты</a></li>
					</ul>
				</div>
				<div class="cell small-6 medium-2 margin-bottom-1">
					<h6 class="margin-bottom-1">Поставщикам</h6>
					<ul class="menu vertical">
						<li><a href="{{ route('page.view', ['products']) }}" title="Продукция">Продукция</a></li>
						<li><a href="{{ route('page.view', ['payment']) }}" title="Оплата">Оплата</a></li>
						<li><a href="{{ route('page.view', ['info']) }}" title="Информация">Информация</a></li>
					</ul>
				</div>
				<div class="cell small-6 medium-3 medium-offset-3">
					<h6 class="margin-bottom-1">Контактная информация</h6>
					{{ config('settings.phone') }}<br/>
					{{ config('settings.address') }}
				</div>
			</div>
		</div>
	</div>
	<div class="green-container">
		<div class="grid-container">
			<div class="cell small-12 text-center">
				ООО &laquo;Меридиан&raquo; 2018 {{ date('Y') > 2018 ? '-' . date('Y') : '' }}
			</div>
		</div>
	</div>
</footer>