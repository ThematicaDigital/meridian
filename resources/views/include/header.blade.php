<header>
	<div class="green-container">
		<div class="grid-container">
			<div class="cell small-12 text-right">
				{{ config('settings.address') }}
				<a href="tel:{{ config('settings.phone') }}" class="display-inline-block margin-left-2">
                    {{ config('settings.phone') }}
                </a>
			</div>
		</div>
	</div>
	<div class="grid-container">
		<div class="top-bar">
            <div class="top-bar-left hide-for-small-only">
            	<a href="{{ route('home') }}" title="Эко Мир">
            		<img src="/images/logotype.svg" title="Эко Мир" alt="Эко Мир" />
            	</a>
            </div>
            <div class="top-bar-right">
            	<div class="text-right tagline hide-for-small-only">
            		<img src="/images/subtag.svg" class="margin-right-1" title="Эко Мир" alt="Эко Мир"/>
            	</div>
            	<nav>
                    <ul class="menu flex-container">
                        <li class="align-middle flex-container {{{ (Request::is('/') ? 'active' : '') }}}">
                            <a href="{{ route('home') }}" class="text-uppercase" title="Эко Мир">Главная</a>
                        </li>
                        <li class="align-middle flex-container {{{ (Request::is('page/about') ? 'active' : '') }}}">
                            <a href="{{ route('page.view', ['about']) }}" class="text-uppercase" title="О компании">О компании</a>
                        </li>
                        <li class="align-middle flex-container {{{ (Request::is('page/products') ? 'active' : '') }}}">
                            <a href="{{ route('page.view', ['products']) }}" class="text-uppercase" title="Продукция">Продукция</a>
                        </li>
                        <li class="align-middle flex-container {{{ (Request::is('page/for-suppliers') ? 'active' : '') }}}">
                            <a href="{{ route('page.view', ['for-suppliers']) }}" class="text-uppercase" title="Поставщикам">Поставщикам</a>
                        </li>
                        <li class="align-middle flex-container {{{ (Request::is('page/for-buyers') ? 'active' : '') }}}">
                            <a href="{{ route('page.view', ['for-buyers']) }}" class="text-uppercase" title="Покупателям">Покупателям</a>
                        </li>
                        <li class="align-middle flex-container {{{ (Request::is('contacts') ? 'active' : '') }}}">
                            <a href="{{ route('contacts') }}" class="text-uppercase" title="Контакты">Контакты</a>
                        </li>
                    </ul>
                </nav>
        	</div>
        </div>
	</div>
</header>