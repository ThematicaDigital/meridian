@extends('layouts.design')

@section('content')
	<div class="slider margin-0">
		<sliderblock></sliderblock>
	</div>
	<div class="grid-container margin-vertical-2">
		<div class="grid-x grid-padding-x">
			<div class="cell">
                <nav role="navigation">
                    {!! Breadcrumbs::render('contacts') !!}
                </nav>
            </div>
        </div>
        <article>
        	<div class="grid-x grid-padding-x">
				<div class="cell small-12">
					<h1 class="text-center margin-vertical-2">Контакты</h1>
				</div>
				<div class="cell small-12 medium-3 margin-bottom-1">
					<div class="margin-bottom-2">
						<p class="margin-bottom-0">Бесплатный звонок по России</p>
						<a href="tel:88001001321" class="green-text">8-800-100-1-321</a>
					</div>
					<div class="margin-bottom-2">
						<p class="margin-bottom-1"><strong>Филиал в Москве</strong></p>
						<p class="margin-bottom-0">125009, г. Москва, Вознесенский пер, 11/1</p>
						<p class="margin-bottom-0">Тел./факс: +7(495)988-30-61</p>
						<p class="margin-bottom-0">Лицензия ЦБ РФ 1810</p>
					</div>
					<div class="margin-bottom-2">
						<p class="margin-bottom-1"><strong>Головной офис</strong></p>
						<p class="margin-bottom-0">675000, Амурская область,</p>
						<p class="margin-bottom-0">г. Благовещенск, ул. Амурская, 225</p>
					</div>
				</div>
				<div class="cell small-12 medium-9 margin-bottom-1 text-right">
					<a class="dg-widget-link" href="http://2gis.ru/blagoveshensk/firm/70000001007355341/center/127.565395,50.311956/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Благовещенска</a>

					<div class="dg-widget-link"><a href="http://2gis.ru/blagoveshensk/center/127.565395,50.311956/zoom/16/routeTab/rsType/bus/to/127.565395,50.311956╎Эковид, ООО, производственно-торговая компания?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Эковид, ООО, производственно-торговая компания</a></div>

					<script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script>
					<script charset="utf-8">
						new DGWidgetLoader({"width":640,"height":400,"borderColor":"#a3a3a3","pos":{"lat":50.311956,"lon":127.565395,"zoom":16},"opt":{"city":"blagoveshensk"},"org":[{"id":"70000001007355341"}]});
					</script>

					<noscript style="color:#c00;font-size:16px;font-weight:bold;">
						Виджет карты использует JavaScript. Включите его в настройках вашего браузера.
					</noscript>
				</div>
			</div>
		</article>
	</div>
@endsection