@extends('layouts.design')

@section('content')
	<div class="slider margin-0">
		<sliderblock></sliderblock>
	</div>
	<div class="grid-container margin-vertical-2">
		<div class="grid-x grid-padding-x">
			<div class="cell small-12 medium-3 text-center margin-bottom-1">
				<img src="/images/icon1.svg" class="margin-bottom-1 width-50">
				<h5>Ассортимент</h5>
				<p>Мы постоянно работаем над расширением ассортимента</p>
			</div>
			<div class="cell small-12 medium-3 text-center margin-bottom-1">
				<img src="/images/icon2.svg" class="margin-bottom-1 width-50">
				<h5>Доставка</h5>
				<p>Мы постоянно работаем над расширением ассортимента</p>
			</div>
			<div class="cell small-12 medium-3 text-center margin-bottom-1">
				<img src="/images/icon3.svg" class="margin-bottom-1 width-50">
				<h5>Скидки</h5>
				<p>Мы постоянно работаем над расширением ассортимента</p>
			</div>
			<div class="cell small-12 medium-3 text-center margin-bottom-1">
				<img src="/images/icon4.svg" class="margin-bottom-1 width-50">
				<h5>Гибкие условия</h5>
				<p>Мы постоянно работаем над расширением ассортимента</p>
			</div>
		</div>
	</div>
	<div class="grid-container margin-vertical-3">
		<div class="grid-x grid-padding-x">
			<div class="cell small-12 medium-7 margin-bottom-1">
				<article>
					<h1>ООО &laquo;Меридиан&raquo;</h1>
					<p>Наш сайт является онлайн-площадкой компании ООО «Меридиан», и предназначен для ведения совместного бизнеса в сфере снабжения продуктами питания предприятий общепита (ресторанов, баров, кафе, столовых и т.д), а также для более удобного общения с нашими партнёрами и контрагентами.</p>
					<p><strong>Наши преимущества:</strong></p>
					<p>
						<ul>
							<li>большой выбор продуктов питания для предприятий общественного питания;</li>
							<li>взаимовыгодная система скидок и спецпредложений для постоянных закупщиков;</li>
							<li>оперативная помощь в комплектации заявок на снабжение продуктами;</li>
							<li>оптовая и розничная продажа продуктов питания по всей Москве и области;</li>
							<li>доставка продуктов в рестораны, бары, кафе, столовые.</li>
						</ul>
					</p> 
					<p>Наш сайт является онлайн-площадкой компании ООО «Меридиан», и предназначен для ведения совместного бизнеса в сфере снабжения продуктами питания предприятий общепита (ресторанов, баров, кафе, столовых и т.д), а также для более удобного общения с нашими партнёрами и контрагентами.</p>
				</article>
			</div>
			<div class="cell small-12 medium-5 margin-bottom-1 text-right">
				<img src="/images/discount.jpg" >
			</div>
		</div>
	</div>
	<div class="grid-container margin-vertical-3">
		<div class="grid-x grid-padding-x">
			<div class="cell small-12 margin-bottom-1 text-center text-uppercase">
				<h2>Бренды</h2>
			</div>
			<div class="cell small-12 margin-bottom-1">
				<brands></brands>
			</div>
		</div>
	</div>
	<div class="grid-container margin-vertical-3">
		<div class="grid-x grid-padding-x">
			<div class="cell small-12 margin-bottom-1 text-center text-uppercase">
				<h2>Наши сертификаты</h2>
			</div>
			<div class="cell small-12 margin-bottom-1">
				<brands></brands>
			</div>
		</div>
	</div>

@endsection