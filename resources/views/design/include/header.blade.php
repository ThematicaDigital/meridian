<header>
	<div class="green-container">
		<div class="grid-container">
			<div class="cell small-12 text-right">
				г. Благовещенск, ул. Ленина 123, офис 7
				<a href="tel:88001234567" class="display-inline-block margin-left-2">8 880 123 4567</a>
			</div>
		</div>
	</div>
	<div class="grid-container">
		<div class="top-bar">
            <div class="top-bar-left hide-for-small-only">
            	<a href="{{ route('design.index') }}">
            		<img src="/images/logotype.svg" />
            	</a>
            </div>
            <div class="top-bar-right">
            	<div class="text-right tagline hide-for-small-only">
            		<img src="/images/subtag.svg" class="margin-right-1"/>
            	</div>
            	<nav>
                    <ul class="menu flex-container">
                        <li class="align-middle flex-container {{{ (Request::is('design') ? 'active' : '') }}}">
                            <a href="{{ route('design.index') }}" class="text-uppercase">Главная</a>
                        </li>
                        <li class="align-middle flex-container {{{ (Request::is('design/about') ? 'active' : '') }}}">
                            <a href="{{ route('design.about') }}" class="text-uppercase">О компании</a>
                        </li>
                        <li class="align-middle flex-container {{{ (Request::is('design/products') ? 'active' : '') }}}">
                            <a href="{{ route('design.products') }}" class="text-uppercase">Продукция</a>
                        </li>
                        <li class="align-middle flex-container {{{ (Request::is('design/suppliers') ? 'active' : '') }}}">
                            <a href="{{ route('design.suppliers') }}" class="text-uppercase">Поставщикам</a>
                        </li>
                        <li class="align-middle flex-container {{{ (Request::is('design/suppliers') ? 'active' : '') }}}">
                            <a href="{{ route('design.suppliers') }}" class="text-uppercase">Покупателям</a>
                        </li>
                        <li class="align-middle flex-container {{{ (Request::is('design/contacts') ? 'active' : '') }}}">
                            <a href="{{ route('design.contacts') }}" class="text-uppercase">Контакты</a>
                        </li>
                    </ul>
                </nav>
        	</div>
        </div>
	</div>
</header>