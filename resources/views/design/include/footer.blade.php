<footer>
	<div class="yellow-container padding-vertical-2">
		<div class="grid-container">
			<div class="grid-x">
				<div class="cell small-6 medium-2 margin-bottom-2">
					<a href="{{ route('design.index') }}">
	            		<img src="/images/logotype.svg" />
	            	</a>
				</div>
				<div class="cell small-6 medium-2 margin-bottom-2">
					<h6 class="margin-bottom-1">О нас</h6>
					<ul class="menu vertical">
						<li><a>О компании</a></li>
						<li><a>Контакты</a></li>
					</ul>
				</div>
				<div class="cell small-6 medium-2 margin-bottom-1">
					<h6 class="margin-bottom-1">Поставщикам</h6>
					<ul class="menu vertical">
						<li><a>Продукция</a></li>
						<li><a>Оплата</a></li>
						<li><a>Информация</a></li>
					</ul>
				</div>
				<div class="cell small-6 medium-3 medium-offset-3">
					<h6 class="margin-bottom-1">Контактная информация</h6>
					8 800 123 4567<br/>
					г. Благовещенск, ул. Ленина 123
				</div>
			</div>
		</div>
	</div>
	<div class="green-container">
		<div class="grid-container">
			<div class="cell small-12 text-center">
				ООО &laquo;Меридиан&raquo; 2017
			</div>
		</div>
	</div>
</footer>