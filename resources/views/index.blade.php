@extends('layouts.app')

@section('content')
	{!! \App\Http\Controllers\SlidesController::widgetMain() !!}
	<div class="grid-container margin-vertical-2">
		<div class="grid-x grid-padding-x">
			<div class="cell small-6 medium-3 text-center margin-bottom-1">
				<img src="/images/icon1.svg" class="margin-bottom-1 width-50">
				<h5>Ассортимент</h5>
				<p>Мы постоянно работаем над расширением ассортимента</p>
			</div>
			<div class="cell small-6 medium-3 text-center margin-bottom-1">
				<img src="/images/icon2.svg" class="margin-bottom-1 width-50">
				<h5>Доставка</h5>
				<p>Мы постоянно работаем над расширением ассортимента</p>
			</div>
			<div class="cell small-6 medium-3 text-center margin-bottom-1">
				<img src="/images/icon3.svg" class="margin-bottom-1 width-50">
				<h5>Скидки</h5>
				<p>Мы постоянно работаем над расширением ассортимента</p>
			</div>
			<div class="cell small-6 medium-3 text-center margin-bottom-1">
				<img src="/images/icon4.svg" class="margin-bottom-1 width-50">
				<h5>Гибкие условия</h5>
				<p>Мы постоянно работаем над расширением ассортимента</p>
			</div>
		</div>
	</div>
	{!! \App\Http\Controllers\PagesController::preview('main') !!}
	{!! \App\Http\Controllers\BrandsController::widgetMain() !!}
	{!! \App\Http\Controllers\CertificatesController::widgetMain() !!}
@endsection