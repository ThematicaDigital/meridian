@extends('layouts.app')

@section('content')
	{!! \App\Http\Controllers\SlidesController::widgetMain() !!}
	<div class="grid-container margin-vertical-2">
		<div class="grid-x grid-padding-x">
			<div class="cell">
                <nav role="navigation">
                    {!! Breadcrumbs::render('page', $page) !!}
                </nav>
            </div>
			<div class="cell small-12">
	            <article>
						<h1 class="text-center margin-vertical-2 text-uppercase">{{ $page->title }}</h1>
						<p>{!! $page->text !!}</p>
				</article>
			</div>
		</div>
	</div>
	{!! \App\Http\Controllers\CertificatesController::widgetMain() !!}
@endsection