@if (!$brands->isEmpty())
	<div class="grid-container margin-vertical-3">
		<div class="grid-x grid-padding-x">
			<div class="cell small-12 margin-bottom-1 text-center text-uppercase">
				<h2>Бренды</h2>
			</div>
			<div class="cell small-12 margin-bottom-1">
				<brands inline-template>
					<slick ref="slick" :options="slickOptions">
						@foreach($brands as $brand)
				        	<img src="{{ $brand->image }}" alt="{{ $brand->title }}" title="{{ $brand->title }}" >
				        @endforeach
				    </slick>
				</brands>
			</div>
		</div>
	</div>
@endif