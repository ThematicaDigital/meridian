@if (!$certificates->isEmpty())
	<div class="grid-container margin-vertical-3">
		<div class="grid-x grid-padding-x">
			<div class="cell small-12 margin-bottom-1 text-center text-uppercase">
				<h2>Наши сертификаты</h2>
			</div>
			<div class="cell small-12 margin-bottom-1">
				<brands inline-template>
					<slick ref="slick" :options="slickOptions">
						@foreach($certificates as $certificate)
				        	<img src="{{ $certificate->image }}" alt="{{ $certificate->title }}" title="{{ $certificate->title }}" >
				        @endforeach
				    </slick>
				</brands>
			</div>
		</div>
	</div>
@endif